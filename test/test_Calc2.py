import numpy as np
import numpy.testing as npt

import Calc2

def test_Calc2_smoke():
    #Smoke_test
    obt = Calc2.Calc2_object()

def test_Calc2_object_fizz():
    #test the fizz_function
    obj = Calc2.Calc2_object()
    output = obj.fizz()

    npt.assert_equal(output, "buzz")
