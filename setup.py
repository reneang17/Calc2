from distutils.core import setup

setup(name = 'Calc2',
author = 'Rene Angeles',
author_email = 'reneang17@gmail.com',
url = 'https://github.com/reneang17/Calc2',
packages = ['Calc2']
)
